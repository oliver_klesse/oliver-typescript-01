﻿using Tyler.WebApp.Security.Web;

namespace TS01.Security
{
    public class TS01Resources : TylerResources
    {
        public const string Recipes = "Recipes";
        public const string Reviews = "Reviews";
        public const string SimpleDataItems = "SimpleDataItems";
        public const string Parties = "Parties";
    }
}