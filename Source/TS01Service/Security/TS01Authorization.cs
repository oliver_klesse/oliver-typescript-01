﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Thinktecture.IdentityModel.Owin.ResourceAuthorization;
using Tyler.WebApp.Security.Extensions;
using Tyler.WebApp.Security.Web;

namespace TS01.Security
{
    public class TS01Authorization : TylerAuthorizationManager
    {
        public override Task<bool> CheckAccessAsync(ResourceAuthorizationContext context)
        {
            // check claims and throw forbidden to deny access
            // use ClaimsPrincipal Extensions to get to custom claims
            var userId = context.Principal.GetOipUserID();
            if (userId > 99999)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            // check for system user if using Odyssey IDP and bypass all other security checks
            int odysseyUserId = 0;
            int.TryParse(context.Principal.GetClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/userid"), out odysseyUserId);
            if (odysseyUserId == 1)
                return Ok();

            var resource = context.Resource();

            if (resource == TS01Resources.Parties)
            {
                return CheckPartyAccessAsync(context);
            }

            if (resource == TylerResources.Cases)
            {
                return CheckCasesAccessAsync(context);
            }

            return Nok();
        }

        private Task<bool> CheckCasesAccessAsync(ResourceAuthorizationContext context)
        {
            if (!context.Principal.Identity.IsAuthenticated)
            {
                return Nok();
            }

            var action = context.Action();
            if (action == TylerResources.Actions.ViewAll)
            {
                return Nok();
            }

            // check all actions for access to they key
            var key = String.Empty;
            if (context.TryGetContextDataKey(out key))
            {
                return CheckAccessByKeyAsync(context, key);
            }

            return Ok();
        }

        private Task<bool> CheckPartyAccessAsync(ResourceAuthorizationContext context)
        {
            if (!context.Principal.Identity.IsAuthenticated)
            {
                return Nok();
            }

            var action = context.Action();
            if (action == TylerResources.Actions.ViewAll)
            {
                return Ok();
            }

            if (action == TylerResources.Actions.View)
            {
                var key = String.Empty;
                if (context.TryGetContextDataKey(out key))
                {
                    return CheckAccessByKeyAsync(context, key);
                }
            }

            if (action == TylerResources.Actions.Edit)
            {
                // check edit rights for this user
            }


            return Ok();
        }

        private Task<bool> CheckAccessByKeyAsync(ResourceAuthorizationContext context, string key)
        {
            // check rights using the context data
            if (key == "1")
            {
                return Eval("portal_admin@yourdomain.com".Equals(
                  context.Principal.Identity.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (key == "5")
            {
                return Nok();
            }

            return Ok();
        }
    }
}