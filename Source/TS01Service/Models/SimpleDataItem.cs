﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TS01Service.Models
{
    // The simplest type of model
    // http://www.asp.net/web-api/overview/odata-support-in-aspnet-web-api/odata-v4/create-an-odata-v4-endpoint
    //
    public class SimpleDataItem
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}