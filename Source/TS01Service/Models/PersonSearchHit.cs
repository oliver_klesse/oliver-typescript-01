﻿
using Tyler.WebApp.Search.Models;

namespace TS01Service.Models
{
    public class PersonSearchHit : SearchHit
    {
        public PersonSearchHit()
        {

        }

        public int id { get { return this.personId; } }
        public int personId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
    }

}