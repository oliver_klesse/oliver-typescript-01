﻿using System.ComponentModel.DataAnnotations;

namespace TS01Service.Models
{
    public class Party
    {
        [Key]
        public int PartyID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}