﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TS01Service.Models
{
    // This model represents a standard type in OData
    //
    // The Web API implementation of OData is designed around IQueryable. This means that delayed execution is utilized so that
    // only collections that have been requested by the OData url are returned. If a model has a collection of standard types, 
    // then they will only be included if the $expand keyword is used in the OData query.
    //
    // Example OData query: http://localhost/app/TS01Service/Recipes(1)?$expand=Reviews
    //
    public class Review
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}