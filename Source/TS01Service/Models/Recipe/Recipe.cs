﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.OData.Builder;

namespace TS01Service.Models
{
    // Model that demonstrates containment in OData v4
    //
    // http://www.asp.net/web-api/overview/odata-support-in-aspnet-web-api/odata-v4/odata-containment-in-web-api-22
    // "Traditionally, an entity could only be accessed if it were encapsulated inside an entity set. But OData v4 
    //  provides two additional options, Singleton and Containment, both of which WebAPI 2.2 supports."
    //
    public class Recipe
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public ICollection<Ingredient> Ingredients { get; set; }
        [Contained]
        public ICollection<Review> Reviews { get; set; }
    }
}