﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TS01Service.Models
{
    // This model represents a complex type in OData
    // A complex type in OData is a model that does not have a key value. 
    //
    // The Web API implementation of OData is designed around IQueryable. This means that delayed execution is utilized so that
    // only collections that have been requested by the OData url are returned. A complex type is one that does not
    // have a key value, and thus cannot be queried in the same was as a standard type. 
    //
    // If there is a collection of complex types, then they will always be included in the results. A standard type
    // will only be included if the $expand keyword is used in the OData query.
    //
    public class Ingredient
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}