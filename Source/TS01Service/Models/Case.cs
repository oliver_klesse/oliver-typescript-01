﻿using System.ComponentModel.DataAnnotations;

namespace TS01Service.Models
{
    public class Case
    {
        [Key]
        public int CaseID { get; set; }

        public string CaseNumber { get; set; }
        public string CaseStyle { get; set; }
    }
}