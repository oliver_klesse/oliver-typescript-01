﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TS01Service.Models
{
    public class Email
    {
        public Email()
        {

        }

        public int? emailId { get; set; }
        public string mailBox { get; set; }
        public string domain { get; set; }
        public string fullEmailAddress { get; set; }
    }

}