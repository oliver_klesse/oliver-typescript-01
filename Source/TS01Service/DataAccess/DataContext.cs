﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TS01Service.Models;

namespace TS01Service.DataAccess
{
    public class DataContext
    {
        public IQueryable<SimpleDataItem> SimpleDataItems
        {
            get
            {
                return new List<SimpleDataItem>() {
          new SimpleDataItem { Id = 1, FirstName = "Apple", LastName ="Acorn" },
          new SimpleDataItem { Id = 2, FirstName = "Banana", LastName = "Bulb"},
          new SimpleDataItem { Id = 3, FirstName = "Citrus", LastName = "Clover"}
        }.AsQueryable();
            }
        }


        public IQueryable<Recipe> Recipes
        {
            get
            {
                return new List<Recipe>() {
          new Recipe {
            Id = 1,
            Description = "Apple Pie",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Provides the sweatness" },
              new Ingredient { Name = "Butter", Description = "Yum" },
              new Ingredient { Name = "Apples", Description = "Only enough to qualify the name" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Best Apple pie ever!" },
              new Review { Id = 2, Text = "Like mom used to make" }
            }
          },
          new Recipe {
            Id = 2,
            Description = "Peach Cobbler",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Just enough to make your mouth water" },
              new Ingredient { Name = "Butter", Description = "Does not need qualification" },
              new Ingredient { Name = "Peaches", Description = "Fresh peaches" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Peachy!" },
            }
          },
          new Recipe {
            Id = 3,
            Description = "Fried Chicken",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Just enough to make your mouth water" },
              new Ingredient { Name = "Butter", Description = "Does not need qualification" },
              new Ingredient { Name = "Peaches", Description = "Fresh peaches" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Peachy!" },
            }
          },
          new Recipe {
            Id = 4,
            Description = "Gumbo",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Just enough to make your mouth water" },
              new Ingredient { Name = "Butter", Description = "Does not need qualification" },
              new Ingredient { Name = "Peaches", Description = "Fresh peaches" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Peachy!" },
            }
          },
          new Recipe {
            Id = 5,
            Description = "Sushi Fever",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Just enough to make your mouth water" },
              new Ingredient { Name = "Butter", Description = "Does not need qualification" },
              new Ingredient { Name = "Peaches", Description = "Fresh peaches" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Peachy!" },
            }
          },
          new Recipe {
            Id = 6,
            Description = "Pizza",
            Ingredients = new List<Ingredient> {
              new Ingredient { Name = "Flour", Description = "Contains wheat" },
              new Ingredient { Name = "Sugar", Description = "Just enough to make your mouth water" },
              new Ingredient { Name = "Butter", Description = "Does not need qualification" },
              new Ingredient { Name = "Peaches", Description = "Fresh peaches" }
            },
            Reviews = new List<Review> {
              new Review { Id = 1, Text = "Peachy!" },
            }
          }
        }.AsQueryable();
            }
        }

    }
}