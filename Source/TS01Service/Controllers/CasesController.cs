﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using Thinktecture.IdentityModel.WebApi;
using Tyler.WebApp.Security.Web;
using Tyler.WebApp.Security.Extensions;
using TS01Service.Models;

namespace TS01Service.Controllers
{
    [ResourceAuthorize(TylerResources.Actions.View, TylerResources.Cases)]
    public class CasesController : ODataController
    {
        // GET  /Cases
        [EnableQuery]
        //[ResourceAuthorize(TylerResources.Actions.ViewAll, TylerResources.Cases)]
        [AllowAnonymous]
        public IHttpActionResult Get()
        {
            return NotFound();
        }

        // GET  /Cases({id})
        [EnableQuery]
        [ResourceAuthorize(TylerResources.Actions.View, TylerResources.Cases)]
        public SingleResult<Case> Get([FromODataUri] int key)
        {
            // if you need a claim for authorization, put that in the TS01Authorization class

            // if you need a claim to filter data, access it from the ClaimsPrincipal
            var cp = (ClaimsPrincipal)User;
            var claimByName = cp.GetClaim("ClaimName");
            var claimByExtensionMethod = cp.GetOipUserID();

            var result = new List<Case>
      {
        new Case
        {
          CaseID = key,
          CaseNumber = "BV-123490-NJ",
          CaseStyle = "Dale Smith vs State of TX"
        }
      }.AsQueryable();
            return SingleResult.Create(result);
        }

        // POST  /Cases
        [ResourceAuthorize(TylerResources.Actions.Add, TylerResources.Cases)]
        public async Task<IHttpActionResult> Post([FromBody] Case c)
        {
            c.CaseID = 75371;
            // return the newly saved hearing
            return Created(c);
        }
    }
}