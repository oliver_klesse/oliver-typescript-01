﻿using System.Web.Http;
using System.Web.OData;
using Thinktecture.IdentityModel.WebApi;
using Tyler.WebApp.Security.Web;

namespace TS01Service.Controllers
{
    [ResourceAuthorize(TylerResources.Actions.View, TylerResources.Cases)]
    public class PingController : ApiController
    {
        [EnableQuery]
        [AllowAnonymous]
        [Route("~/ping")]
        public IHttpActionResult Get()
        {
            return Ok();
        }
    }
}