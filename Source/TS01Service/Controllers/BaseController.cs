﻿using Nest;
using System.Web.OData;

namespace TS01Service.Controllers
{
    public class BaseController : ODataController
    {
        protected ElasticClient SearchClient
        {
            get
            {
                if (_searchClient == null)
                {
                    var settings = new ConnectionSettings(WebConfigHelper.ElasticSearchUri, "trainingIndex");
                    settings.ThrowOnElasticsearchServerExceptions(); // validate elastic server connection
                    _searchClient = new ElasticClient(settings);
                }
                return _searchClient;
            }
        }
        private ElasticClient _searchClient;

    }
}