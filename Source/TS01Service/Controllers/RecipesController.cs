﻿using System.Linq;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using TS01Service.DataAccess;
using TS01Service.Models;

namespace TS01Service.Controllers
{
    public class RecipesController : ODataController
    {
        private readonly DataContext ctx = new DataContext();
        // GET  /Recipes
        [EnableQuery]
        [AllowAnonymous]
        public IQueryable<Recipe> Get()
        {
            return ctx.Recipes;
        }

        // GET  /Recipes({key})
        [EnableQuery]
        [AllowAnonymous]
        public SingleResult<Recipe> Get([FromODataUri] int key)
        {
            var result = ctx.Recipes.Where(item => item.Id == key);

            return SingleResult.Create(result);
        }

        // GET  /Recipes({key})/Reviews
        [EnableQuery]
        [AllowAnonymous]
        public IQueryable<Review> GetReviews([FromODataUri] int key)
        {
            var result = ctx.Recipes.Where(item => item.Id == key).SelectMany(r => r.Reviews);

            return result;
        }

        // GET  /Recipes({key})/Reviews({rKey})
        [ODataRoute("Recipes({key})/Reviews({rKey})")]
        [AllowAnonymous]
        public IHttpActionResult GetReviews(int key, [FromODataUri] int rKey)
        {
            var result =
              ctx.Recipes.Where(item => item.Id == key).Select(r => r.Reviews.Where(v => v.Id == rKey)).FirstOrDefault();

            return Ok(result);
        }
    }
}