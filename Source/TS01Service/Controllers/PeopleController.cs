﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using TS01Service.Models;

namespace TS01Service.Controllers
{
    public class PeopleController : BaseController
    {

        [EnableQuery]
        public SingleResult<Person> Get([FromODataUri] int key)
        {
            // --------------------
            // Query Elastic-Search
            // --------------------

            ISearchResponse<Person> searchResponse = SearchClient.Search<Person>(
                s => s.Index(WebConfigHelper.IndexName).From(0).Size(1)
                .Query(q => q.Term(p => p.personId, key.ToString())));

            if ((searchResponse != null) && (searchResponse.Hits != null))
            {
                Person person = searchResponse.Hits.First().Source;

                // ---------------
                // Resolve intents
                // ---------------

                // Future Enhancement...

                // ---------------------------
                // Return result as IQueriable
                // ---------------------------

                List<Person> people = new List<Person> { person };
                return SingleResult.Create(people.AsQueryable());
            }

            // -----------------------------
            // Otherwise, return a 404 error
            // -----------------------------

            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.NotFound,
                String.Format("Could not retrieve Person with personId = {0}", key)));
        }

    }

}