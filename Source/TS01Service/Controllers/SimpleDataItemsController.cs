﻿using System.Linq;
using System.Web.Http;
using System.Web.OData;
using TS01Service.DataAccess;
using TS01Service.Models;

namespace TS01Service.Controllers
{
    public class SimpleDataItemsController : ODataController
    {
        private readonly DataContext ctx = new DataContext();
        // GET  /SimpleDataItems
        [EnableQuery]
        [AllowAnonymous]
        public IQueryable<SimpleDataItem> Get()
        {
            return ctx.SimpleDataItems;
        }

        // GET  /SimpleDataItems({key})
        [EnableQuery]
        [AllowAnonymous]
        public SingleResult<SimpleDataItem> Get([FromODataUri] int key)
        {
            var result = ctx.SimpleDataItems.Where(item => item.Id == key);

            return SingleResult.Create(result);
        }
    }
}