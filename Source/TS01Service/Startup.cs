﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TS01Service.Startup))]
namespace TS01Service
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureWebApi(app);
        }
    }
}