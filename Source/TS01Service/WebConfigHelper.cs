﻿using System;
using System.Web.Configuration;

namespace TS01Service
{
    /// <summary>
    /// Returns values from the Service Web.config.
    /// </summary>
    public class WebConfigHelper
    {
        /// <summary>
        /// Returns the Uri at which to find the Elastic-Search server.
        /// </summary>
        public static Uri ElasticSearchUri
        {
            get
            {
                try
                {
                    string uri = WebConfigurationManager.AppSettings["ElasticSearchUri"] ?? string.Empty;
                    if (String.IsNullOrWhiteSpace(uri)) throw new Exception("Missing or blank \"ElasticSearchUri\" app-setting in web.config.");
                    return new Uri(uri);
                }
                catch (UriFormatException ex)
                {
                    throw new Exception("Configuration error - Elastic Search Uri is invalid.", ex);
                }
            }
        }

        /// <summary>
        /// Returns the site-Id, used for generating the Elastic-Search index-name we're querying against.
        /// </summary>
        public static string SiteId
        {
            get
            {
                try
                {
                    string siteId = WebConfigurationManager.AppSettings["SiteID"] ?? string.Empty;
                    if (String.IsNullOrWhiteSpace(siteId)) throw new Exception("Missing or blank \"SiteID\" app-setting in web.config.");
                    return siteId;
                }
                catch (UriFormatException ex)
                {
                    throw new Exception("Configuration error - Site Id is invalid.", ex);
                }
            }
        }

        /// <summary>
        /// Returns the Elastic-Search index-name we're querying against.
        /// </summary>
        public static string IndexName
        {
            get { return WebConfigHelper.SiteId.ToLower() + "_trainingindex"; }
        }
    }

}