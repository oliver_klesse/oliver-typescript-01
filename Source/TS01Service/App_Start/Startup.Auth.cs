﻿using System;
using System.Configuration;
using Microsoft.Owin;
using Owin;
using TS01.Security;

namespace TS01Service
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseResourceAuthorization(new TS01Authorization());
        }
    }
}