﻿using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Owin;
using Tyler.WebApp.Security.Web;
using TS01Service.Models;
using Tyler.WebApp.Search.Models;

namespace TS01Service
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes(); //NB Must come before OData route mapping
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            // OData
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.Ignore<PersonSearchHit>();
            builder.EntitySet<IndexSearch>("Search");
            builder.EntitySet<Person>("People");

            builder.EntitySet<SimpleDataItem>("SimpleDataItems");
            builder.EntitySet<Recipe>("Recipes");
            builder.EntitySet<Case>("Cases");
            builder.EntitySet<Party>("Parties");
            config.MapODataServiceRoute("ODataRoute", null, builder.GetEdmModel());

            //WebApiConfigBearerToken.Configure(config);

            app.UseWebApi(config);
        }
    }
}