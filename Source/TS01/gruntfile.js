'use strict';
module.exports = function (grunt) {
    grunt.initConfig({
        //pkg: grunt.file.readJSON('package.json'),
        nggettext_extract: {
            options: {
                extensions: {
                    htm: 'html',
                    html: 'html',
                    php: 'html',
                    phtml: 'html',
                    tml: 'html',
                    js: 'js',
                    cshtml: 'html'
                }
            },
            pot: {
                files: {
                    'po/template.pot': [
                        '**/Areas/**/*.html',
                        '**/Areas/**/*.cshtml',
                        '**/Areas/**/*.js',
                        '**/Views/**/*.html',
                        '**/Views/**/*.cshtml',
                        '**/Views/**/*.js'
                    ]
                }
            }
        },
        nggettext_compile: {
            all: {
                files: {
                    'po/translations.js': ['po/*.po']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-angular-gettext');

    grunt.registerTask('default', ['nggettext_extract', 'nggettext_compile'])
};
