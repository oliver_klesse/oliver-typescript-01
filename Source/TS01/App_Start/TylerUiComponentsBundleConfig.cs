﻿using System.Web.Optimization;
using WebActivatorEx;

[assembly: PostApplicationStartMethod(typeof(TylerUI.TylerUiComponentsBundleConfig), "RegisterTylerUiComponentsBundles")]
namespace TylerUI
{
    public class TylerUiComponentsBundleConfig
    {
        public static void RegisterTylerUiComponentsBundles()
        {
            var componentUrl = "~/Scripts/tylerui/components/";
            var tylerUiComponentsJsBundle = new ScriptBundle("~/bundles/TylerUiComponentsJs");
            tylerUiComponentsJsBundle.Include(
              componentUrl + "tt-components.js",
              componentUrl + "tt-notes.js",
              componentUrl + "tt-btn-group.js",
              componentUrl + "tt-timeline.js",
              componentUrl + "tt-smooth-scroll.js",
              componentUrl + "tt-focus.js",
              componentUrl + "tt-focus-manager.js",
              componentUrl + "tt-focus-group.js",
              componentUrl + "tt-focus-container.js",
              componentUrl + "tt-focus-item.js",
              componentUrl + "tt-label-value.js",
              componentUrl + "tt-filters.js",
              componentUrl + "tt-steps.js",
              componentUrl + "tt-pagination.js",
              componentUrl + "tt-calendar.js",
              componentUrl + "tt-facet.js",
              "~/bower_components/Chart.js/Chart.js",
              "~/bower_components/angular-chart.js/angular-chart.js"
              );

            BundleTable.Bundles.Add(tylerUiComponentsJsBundle);

        }
    }
}