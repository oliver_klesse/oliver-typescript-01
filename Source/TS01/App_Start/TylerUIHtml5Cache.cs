﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Hosting;
//[assembly: PostApplicationStartMethod(typeof(TylerUI.TylerUIHtml5Cache), "RegisterTylerUIHtml5Cache")]
namespace TylerUI
{
    public class AppCacheModel
    {
        public AppCacheModel()
        {
            AssemblyVersion = GetType().Assembly.GetName().Version.ToString();
            CacheCollection = new List<string>();

            // tyler ui
            AddBundle("~/bundles/TylerUiCss");
            AddBundle("~/bundles/ModernizrDefunctr");
            AddBundle("~/bundles/TylerUiJsDefer");
            AddBundle("~/bundles/TylerUiComponentsJs");
            AddFiles("~/Scripts", "jsnlog.min.js");
            AddFiles("~/Scripts/tylerui", "*.html");
            AddFiles("~/Scripts/tylerui/components", "*.html");
            AddFiles("~/Content/tylerui", "tylerui-browser-requirements.css");
            AddFiles("~/Content/tylerui/images", "*");
            AddFiles("~/Content/tylerui/svgs", "*");
            AddFiles("~/Content/tylerui/images/browsers", "*");
            AddFiles("~/Views/Shared", "*.html");

            // bundled location of fonts is here ~/fonts
            AddFiles("~/fonts", "*");

            AddAppResourcesToManifest();
        }
        public string AssemblyVersion { get; set; }
        public List<string> CacheCollection { get; set; }

        public virtual void AddAppResourcesToManifest()
        {
        }
        public void AddBundle(string bundleName)
        {
            CacheCollection.Add(WriteBundle(bundleName));
        }
        public void AddFiles(string virtualPath, string fileNamePattern, string cdn = null)
        {
            CacheCollection.Add(GetPhysicalFilesToCache(virtualPath, fileNamePattern, cdn));
        }
        private string WriteBundle(string virtualPath)
        {
            return System.Web.Optimization.Scripts.Url(virtualPath).ToString();
        }

        private string GetPhysicalFilesToCache(string relativeFolderToAssets, string fileTypes, string cdnBucket)
        {
            var virtualPath = HttpRuntime.AppDomainAppVirtualPath;
            var outputString = new StringBuilder();
            var folder = new DirectoryInfo(HostingEnvironment.MapPath(relativeFolderToAssets));
            foreach (FileInfo file in folder.GetFiles(fileTypes))
            {
                string location = !String.IsNullOrEmpty(cdnBucket) ? cdnBucket : relativeFolderToAssets;
                string outputFileName = (location + "/" + file)
                .Replace("~", virtualPath)
                .Replace("//", "/");
                outputString.AppendLine(outputFileName);
            }
            return outputString.ToString();
        }
    }
    public class TylerUIHtml5Cache
    {
        public static void RegisterTylerUIHtml5Cache()
        {
        }

        internal static string BuildManifestResponse(HttpResponseBase Response, TylerUI.AppCacheModel model)
        {
            Response.ContentType = "text/cache-manifest";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.MinValue);
            //mvc is adding a blank line on the page, so don't use cshtml for this, just build a string
            //return View("~/Views/Shared/_TylerUiAppManifest.cshtml", model);

            var sb = new StringBuilder();
            sb.AppendLine("CACHE MANIFEST");

            sb.AppendLine(String.Empty);
            sb.Append("# Server Assembly Version: ");
            sb.AppendLine(model.AssemblyVersion);

            sb.AppendLine(String.Empty);
            sb.AppendLine("NETWORK:");
            sb.AppendLine("*");

            sb.AppendLine(String.Empty);
            sb.AppendLine("CACHE:");
            foreach (string cacheItem in model.CacheCollection)
            {
                sb.AppendLine(cacheItem);
            }

            return sb.ToString();
        }
    }
}
