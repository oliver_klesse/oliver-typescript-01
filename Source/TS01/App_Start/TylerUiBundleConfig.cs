﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using WebActivatorEx;

[assembly: PostApplicationStartMethod(typeof(TylerUI.TylerUiBundleConfig), "RegisterTylerUiBundles")]
namespace TylerUI
{
    public class TylerUiBundleConfig
    {
        public static void RegisterTylerUiBundles()
        {
            var modernizrDefunctrBundle = new ScriptBundle("~/bundles/ModernizrDefunctr");
            modernizrDefunctrBundle.Include("~/Scripts/modernizr-2.8.3.js",
              "~/Scripts/tylerui/modernizrExt.cookies.js",
              "~/Scripts/Vendor/Defunctr/defunctr-1.1.1.js");

            //var tylerUiJsBundle = new ScriptBundle("~/bundles/TylerUiJs");
            //tylerUiJsBundle.Include(
            //  );

            var tylerUiJsBundleDefer = new ScriptBundle("~/bundles/TylerUiJsDefer");
            tylerUiJsBundleDefer.Include(
              "~/bower_components/jquery/dist/jquery.js",
              "~/bower_components/angular-loader/angular-loader.js",
              "~/bower_components/toastr/toastr.js",
              "~/bower_components/mousetrap/mousetrap.js",
              "~/bower_components/fastclick/lib/fastclick.js",
              "~/bower_components/angular/angular.js",
              "~/bower_components/angular-aria/angular-aria.js",
              "~/bower_components/angular-animate/angular-animate.js",
              "~/bower_components/angular-cookies/angular-cookies.js",
              "~/bower_components/angular-message-format/angular-message-format.js",
              "~/bower_components/angular-messages/angular-messages.js",
              //"~/bower_components/angular-mocks/angular-mocks.js",
              "~/bower_components/angular-resource/angular-resource.js",
              "~/bower_components/angular-route/angular-route.js",
              "~/bower_components/angular-sanitize/angular-sanitize.js",
              //"~/bower_components/angular-scenario/angular-scenario.js",
              "~/bower_components/angular-touch/angular-touch.js",
              "~/bower_components/angular-material/angular-material.js",
              "~/bower_components/angular-material/angular-material-mocks.js",
              "~/bower_components/angular-gettext/dist/angular-gettext.js",
              "~/bower_components/angular-formly/dist/formly.js",
              "~/bower_components/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.js",
              "~/bower_components/moment/min/moment-with-locales.js",
              "~/bower_components/mdPickers/dist/mdPickers.js",
              "~/bower_components/appcache-nanny/appcache-nanny.js",
              // please.js no NuGet or Bower so manage updates via TylerUI https://github.com/wingify/please.js/tree/master/src
              "~/Scripts/tylerui/please.js",
              "~/Scripts/tylerui/angular-filter.js",
              "~/Scripts/ngScrollSpy.js",
              "~/Scripts/df-tab-menu.js",
              "~/po/translations.js",
              "~/Scripts/tylerui/tt.init.js",
              "~/Scripts/tylerui/tt-theme.js",
              "~/Scripts/tylerui/tt.autofocus.js",
              "~/Scripts/tylerui/tt-busy-indicator.js",
              "~/Scripts/tylerui/tt-locale.js",
              "~/Scripts/tylerui/tt.service.notification.js",
              "~/Scripts/tylerui/tt-auth.js",
              "~/Scripts/tylerui/tt-matchmedia.js",
              "~/Scripts/tylerui/hotkeys.js",
              "~/Scripts/tylerui/tt-cache-control.js"
              );

            //var tylerUiJsBundleAsync = new ScriptBundle("~/bundles/TylerUiJsAsync");
            //tylerUiJsBundleAsync.Include(
            //  );

            var tylerUiCssBundle = new StyleBundle("~/bundles/TylerUiCss");

            // Toastr and Font Awesome
            tylerUiCssBundle.Include("~/bower_components/toastr/toastr.css", new VirtualPathCssRewriteUrlTransform());
            tylerUiCssBundle.Include("~/bower_components/components-font-awesome/css/font-awesome.css", new VirtualPathCssRewriteUrlTransform());

            // Material Design css
            tylerUiCssBundle.Include("~/bower_components/angular-material/angular-material.css", new VirtualPathCssRewriteUrlTransform());
            tylerUiCssBundle.Include("~/bower_components/mdi/css/materialdesignicons.css", new VirtualPathCssRewriteUrlTransform());
            tylerUiCssBundle.Include("~/bower_components/mdPickers/dist/mdPickers.css", new VirtualPathCssRewriteUrlTransform());
            tylerUiCssBundle.Include("~/Content/tylerui/tylerui.css", new VirtualPathCssRewriteUrlTransform());

            // HotKeys
            tylerUiCssBundle.Include("~/Content/tylerui/hotkeys.css", new VirtualPathCssRewriteUrlTransform());

            // Add2Home https://github.com/cubiq/add-to-homescreen
            //tylerUiCssBundle.Include("~/Content/tylerui/addtohomescreen.css", new VirtualPathCssRewriteUrlTransform());

            BundleCollection bundles = BundleTable.Bundles;
            bundles.Add(modernizrDefunctrBundle);
            //bundles.Add(tylerUiJsBundle);
            //bundles.Add(tylerUiJsBundleAsync);
            bundles.Add(tylerUiJsBundleDefer);
            bundles.Add(tylerUiCssBundle);
        }
    }

    // Custom CSS Rewrite URL Transform class. The built-in one has problems with virtual directories as well as with embedded images (Data URI). 
    // See comments in code below for links to the issues
    public class VirtualPathCssRewriteUrlTransform : IItemTransform
    {
        private static string RebaseUrlToAbsolute(string baseUrl, string url)
        {
            // Fix issue where bunlder tries to resolve embedded data URI images
            // See http://aspnetoptimization.codeplex.com/workitem/88
            if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(baseUrl) || url.StartsWith("/", StringComparison.OrdinalIgnoreCase)
              || url.StartsWith("data:") || url.StartsWith("http://") || url.StartsWith("https://"))
            {
                return url;
            }

            if (!baseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase))
            {
                baseUrl += "/";
            }

            return VirtualPathUtility.ToAbsolute(baseUrl + url);
        }


        private static string ConvertUrlsToAbsolute(string baseUrl, string content)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                return content;
            }

            var regex = new Regex("url\\(['\"]?(?<url>[^)]+?)['\"]?\\)");
            return regex.Replace(content, match => "url(" + RebaseUrlToAbsolute(baseUrl, match.Groups["url"].Value) + ")");
        }


        public string Process(string includedVirtualPath, string input)
        {
            if (includedVirtualPath == null)
            {
                throw new ArgumentNullException("includedVirtualPath");
            }

            // Handle virtual directories
            // Standard CssRewriteUrlTransform does not handle virtual directories properly, so we need custom code to handle this.
            // See http://stackoverflow.com/a/17702773 and http://aspnetoptimization.codeplex.com/workitem/83
            includedVirtualPath = "~" + VirtualPathUtility.ToAbsolute(includedVirtualPath);

            string directory = VirtualPathUtility.GetDirectory(includedVirtualPath.Substring(1));
            return ConvertUrlsToAbsolute(directory, input);
        }

    }

    public static class TylerHtmlHelperExtensions
    {
        public static MvcHtmlString GetLanguages()
        {
            string acceptLanguages = null;

            if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
            {
                acceptLanguages = Newtonsoft.Json.JsonConvert.SerializeObject(HttpContext.Current.Request.UserLanguages);
            }

            return MvcHtmlString.Create(acceptLanguages);
        }
    }

    public static class Scripts
    {
        public static IHtmlString RenderDefer(params string[] paths)
        {
            return System.Web.Optimization.Scripts.RenderFormat(@"<script src='{0}' defer='defer'></script>", paths);
        }
        public static IHtmlString RenderAsync(params string[] paths)
        {
            return System.Web.Optimization.Scripts.RenderFormat(@"<script src='{0}' async></script>", paths);
        }
    }


}
