## Next Steps ##

## Cleanup

Close VS2015 and run `0_Cleanup_Temp_Files_That_VS_Had_Locked.bat` to cleanup the solution directories. 
VS2015 Locks some files during and after project creation, so this allows those to get cleaned up.

### Update NuGet Packages ###
All the missing NuGet packages will be automatically restored when you build your project. 

### Omnibar
Omnibar will need to be configured in order to show at runtime.

* [Configure Omnibar](http://pladvsv-tldr:8080/tylerui/omni/)

### Deployment
Deployment files have been created for you. Visit the .imspkg file located in the Deploy folder and look for 'TODO' to choose answers to the following questions:
  
* Which identity provider configuration you would like to use: Portal, Public or Odyssey?
* Using SiteID in your web.config? You can get that from either a Portal site or Odyssey configuration.


## Helper Scripts ##

There are three Powershell scripts located at the root folder of your web app project (C:\Enterprise\Sprint-2-Training\TS01)

### 1_create_stash_repository.ps1 ###

This script creates a stash (now bitbucket) repo at [http://stash.tylertech.com/projects/WEB](http://stash.tylertech.com/projects/WEB)

### 2_setup_local_git.ps1 ### 

This script creates a stash (now bitbucket) repo under the Web App project.

### 3_create_teamcity_project.ps1 ###

This script will create a TeamCity project for your web app. It includes the following:

* Optionally push the develop and master branches to origin (e.g. stash)
* Auto-build on check in to the dev stream
* Auto-publish on successful build
* Auto-deploy to the Web App build server (pladvsvwebapp)
* Auto-build on check in to the qa stream
* Auto-publish to internal QA streams on successful build

Visit the [WebApps](http://teamcity.tylertech.com/project.html?projectId=WebApps) project in TeamCity 


## Additional Documentation ##

* [http://pladvsv-tldr](http://pladvsv-tldr)
