﻿using System;
using System.Configuration;
using System.Web.Optimization;

namespace TS01.Areas.TS01
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var mockData = Boolean.Parse(ConfigurationManager.AppSettings["mockData"] ?? "false");
            bundles.Add(new ScriptBundle("~/bundles/TS01Js")
            .Include("~/Areas/TS01/Scripts/application.js")
            .Include(mockData ?
                "~/Areas/TS01/Scripts/people.service.mock.js" :
                "~/Areas/TS01/Scripts/people.service.js")
            .Include("~/Areas/TS01/Scripts/people.list.controller.js")
            .Include("~/Areas/TS01/Scripts/people.edit.controller.js")
            .Include("~/Areas/TS01/Scripts/footer.controller.js")
            );

        }
    }
    public class TS01CacheModel : TylerUI.AppCacheModel
    {
        public override void AddAppResourcesToManifest()
        {
            AddBundle("~/bundles/TS01Js");
            AddFiles("~/Areas/TS01/Views/Index", "*.html");
            AddFiles("~/Areas/TS01/Content/images", "*.svg");
        }
    }
}
