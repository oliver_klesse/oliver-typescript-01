﻿

class FooterController {
    footer: Common.IConfig;

    constructor(private appConfig: Common.IConfig) {
        this.footer = appConfig;
    }

}

angular.module('TS01App')
    .controller('footerController', ["appConfig", FooterController]);