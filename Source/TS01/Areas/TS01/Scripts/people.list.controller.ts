﻿import ISimpleDataItem = Models.ISimpleDataItem;

class PeopleListController {
    items: ISimpleDataItem[];
    message: string;
    isBusy: boolean;

    constructor(private $location: ng.ILocationService, private peopleService: PeopleService) {
        this.isBusy = true;
        this.peopleService.getPeople()
            .then((result) => {
                this.isBusy = false;
                this.items = result;
            }, this.setIdle);
    }

    left() {
        return 100 - this.message.length;
    }
    clear() {
        this.message = "";
    }

    setIdle() {
        this.isBusy = false;
    }

    edit(item: ISimpleDataItem) {
        this.$location.path("/edit/" + item.Id.toString(10));
    }
}

angular.module('TS01App')
    .controller('peopleListController', ["$location", "peopleService", PeopleListController]);