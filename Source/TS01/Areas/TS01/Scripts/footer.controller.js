var FooterController = (function () {
    function FooterController(appConfig) {
        this.appConfig = appConfig;
        this.footer = appConfig;
    }
    return FooterController;
})();
angular.module('TS01App')
    .controller('footerController', ["appConfig", FooterController]);
//# sourceMappingURL=footer.controller.js.map