var PeopleService = (function () {
    function PeopleService($http, appConfig, $q) {
        this.$http = $http;
        this.appConfig = appConfig;
        this.$q = $q;
    }
    PeopleService.prototype.getPeople = function () {
        var deferred = this.$q.defer();
        this.$http.get(this.appConfig.serviceUri + "SimpleDataItems")
            .then(function (result) {
            deferred.resolve(result.data.value);
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    };
    PeopleService.prototype.getPerson = function (id) {
        var deferred = this.$q.defer();
        this.$http.get(this.appConfig.serviceUri + ("SimpleDataItems(" + id + ")"))
            .then(function (result) {
            deferred.resolve(result.data);
        }, function (reason) {
            deferred.reject(reason);
        });
        return deferred.promise;
    };
    PeopleService.prototype.savePerson = function (item) {
        // implement http call
    };
    return PeopleService;
})();
angular.module('TS01App')
    .service("peopleService", ["$http", "appConfig", "$q", PeopleService]);
//# sourceMappingURL=people.service.js.map