var PeopleListController = (function () {
    function PeopleListController($location, peopleService) {
        var _this = this;
        this.$location = $location;
        this.peopleService = peopleService;
        this.isBusy = true;
        this.peopleService.getPeople()
            .then(function (result) {
            _this.isBusy = false;
            _this.items = result;
        }, this.setIdle);
    }
    PeopleListController.prototype.left = function () {
        return 100 - this.message.length;
    };
    PeopleListController.prototype.clear = function () {
        this.message = "";
    };
    PeopleListController.prototype.setIdle = function () {
        this.isBusy = false;
    };
    PeopleListController.prototype.edit = function (item) {
        this.$location.path("/edit/" + item.Id.toString(10));
    };
    return PeopleListController;
})();
angular.module('TS01App')
    .controller('peopleListController', ["$location", "peopleService", PeopleListController]);
//# sourceMappingURL=people.list.controller.js.map