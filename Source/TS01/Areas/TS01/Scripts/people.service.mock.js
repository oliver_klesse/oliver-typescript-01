var PeopleServiceMock = (function () {
    function PeopleServiceMock($http, appConfig, $q) {
        this.$http = $http;
        this.appConfig = appConfig;
        this.$q = $q;
    }
    PeopleServiceMock.prototype.getPeople = function () {
        var deferred = this.$q.defer();
        this.createTestData();
        deferred.resolve(this.testData);
        return deferred.promise;
    };
    PeopleServiceMock.prototype.getPerson = function (id) {
        var deferred = this.$q.defer();
        this.createTestData();
        deferred.resolve(this.findTestItem(id));
        return deferred.promise;
    };
    PeopleServiceMock.prototype.savePerson = function (item) {
        // implement http call
    };
    PeopleServiceMock.prototype.createTestData = function () {
        if (!this.testData) {
            this.testData = [
                { FirstName: "First", LastName: "Last", Id: 1 },
                { FirstName: "John", LastName: "Doe", Id: 2 }
            ];
        }
    };
    PeopleServiceMock.prototype.findTestItem = function (id) {
        var item = null;
        angular.forEach(this.testData, function (testItem) {
            if (testItem.Id === id) {
                item = testItem;
            }
        });
        return item;
    };
    return PeopleServiceMock;
})();
angular.module('TS01App')
    .service("peopleService", ["$http", "appConfig", "$q", PeopleServiceMock]);
//# sourceMappingURL=people.service.mock.js.map