﻿declare module Models {
    interface ISimpleDataItem {
        Id: number;
        FirstName: string;
        LastName: string;
        BirthDate?: Date;
    }
}
