angular.module("TS01App", ['ngAria', 'ngRoute', 'TylerUI', 'TylerUI.Components', 'gettext', 'ngScrollSpy'])
    .constant('appConfig', window.appConfig)
    .config([
    '$routeProvider', '$locationProvider', 'appConfig', function ($routeProvider, $locationProvider, appConfig) {
        $routeProvider
            .when('/list', { templateUrl: appConfig.virtualDirectory + 'Areas/TS01/Views/Index/List.html', controller: 'peopleListController', controllerAs: "vm" })
            .when('/edit/:id', { templateUrl: appConfig.virtualDirectory + 'Areas/TS01/Views/Index/Edit.html', controller: 'peopleEditController', controllerAs: "vm" })
            .otherwise({ redirectTo: '/list' });
        $locationProvider.html5Mode(false);
    }
])
    .config([
    '$compileProvider', function ($compileProvider) {
        $compileProvider.debugInfoEnabled(false);
    }
])
    .run([
    'appConfig', 'ttLocale', 'gettextCatalog', '$rootScope', function (appConfig, ttLocale, gettextCatalog, $rootScope) {
        //
        ttLocale.init(gettextCatalog);
        ttLocale.setCurrentLanguage(appConfig.acceptLanguages);
        ttLocale.setDebug(false); //for debug, if true, prepend [MISSING]: to each untranslated string.
    }]);
// bootstrap the app
$(angular.element(document).ready(function () {
    angular.bootstrap(angular.element(window.app), ['TS01App'], {
        // https://docs.angularjs.org/guide/di#using-strict-dependency-injection
        strictDi: false
    });
}));
//# sourceMappingURL=application.js.map