var PeopleEditController = (function () {
    function PeopleEditController($location, $routeParams, peopleService, ttNotificationService, ttLocale) {
        var _this = this;
        this.$location = $location;
        this.peopleService = peopleService;
        this.ttNotificationService = ttNotificationService;
        this.ttLocale = ttLocale;
        this.isBusy = true;
        this.id = parseInt($routeParams["id"]);
        this.peopleService.getPerson(this.id)
            .then(function (result) {
            _this.isBusy = false;
            _this.item = result;
        }, this.setIdle);
    }
    PeopleEditController.prototype.setIdle = function () {
        this.isBusy = false;
    };
    PeopleEditController.prototype.save = function () {
        this.ttNotificationService.info(this.ttLocale.getString('Saved {{first}} {{last}}', { first: this.item.FirstName, last: this.item.LastName }));
        this.$location.path("/list/");
    };
    PeopleEditController.prototype.back = function () {
        this.$location.path("/list/");
    };
    return PeopleEditController;
})();
angular.module('TS01App')
    .controller('peopleEditController', ["$location", "$routeParams", "peopleService", "ttNotificationService", "ttLocale", PeopleEditController]);
//# sourceMappingURL=people.edit.controller.js.map