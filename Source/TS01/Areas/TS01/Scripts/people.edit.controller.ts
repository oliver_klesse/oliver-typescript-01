﻿

class PeopleEditController {
    item: ISimpleDataItem;
    isBusy: boolean;
    private id: number;

    constructor(
        private $location: ng.ILocationService,
        $routeParams: ng.route.IRouteParamsService,
        private peopleService: PeopleService,
        private ttNotificationService: Common.INotificationService,
        private ttLocale: Common.ILocale) {
        this.isBusy = true;
        this.id = parseInt($routeParams["id"]);
        this.peopleService.getPerson(this.id)
            .then((result) => {
                this.isBusy = false;
                this.item = result;
            }, this.setIdle);
    }

    setIdle() {
        this.isBusy = false;
    }
    save() {
        this.ttNotificationService.info(this.ttLocale.getString('Saved {{first}} {{last}}', { first: this.item.FirstName, last: this.item.LastName }));
        this.$location.path("/list/");
    }

    back() {
        this.$location.path("/list/");
    }
}

angular.module('TS01App')
    .controller('peopleEditController', ["$location", "$routeParams", "peopleService", "ttNotificationService", "ttLocale", PeopleEditController]);