﻿declare module Common {
    interface IConfig {
        virtualDirectory: string;
        copyRightDate: number;
        version: string;
        baseUri: string;
        serviceUri: string;
        acceptLanguages: string;
        token: string;
    }

    interface IODataResult<T> {
        value: T
    }

    interface INotificationServiceOptions {
        closeButton: boolean;
        timeOut: number;
        extendedTimeOut: number;
    }

    interface INotificationService {
        info: (message: string, option?: INotificationServiceOptions) => void;
        success: (message: string, option?: INotificationServiceOptions) => void;
        warning: (message: string, option?: INotificationServiceOptions) => void;
        error: (message: string, option?: INotificationServiceOptions) => void;
    }

    interface ILocale {

        init: (gettextCatalog: any) => void;
        setCurrentLanguage: (acceptLanguages: string) => void;
        setDebug: (isDebug: boolean) => void;
        getString: (message: string, parameters?: any) => string;
    }
}