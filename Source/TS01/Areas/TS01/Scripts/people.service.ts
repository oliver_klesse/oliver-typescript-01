﻿
import IODataResult = Common.IODataResult;
class PeopleService {

    constructor(private $http: ng.IHttpService, private appConfig: Common.IConfig, private $q: ng.IQService) {

    }


    getPeople(): ng.IPromise<ISimpleDataItem[]> {
        var deferred = this.$q.defer();
        this.$http.get<IODataResult<ISimpleDataItem[]>>(this.appConfig.serviceUri + "SimpleDataItems")
            .then((result) => {
                deferred.resolve(result.data.value);
            },
            (reason) => {
                deferred.reject(reason);
            });

        return deferred.promise;
    }

    getPerson(id: number): ng.IPromise<ISimpleDataItem> {
        var deferred = this.$q.defer();
        this.$http.get<ISimpleDataItem>(this.appConfig.serviceUri + `SimpleDataItems(${id})`)
            .then((result) => {
                deferred.resolve(result.data);
            },
            (reason) => {
                deferred.reject(reason);
            });
        return deferred.promise;
    }

    savePerson(item: ISimpleDataItem) {
        // implement http call
    }

    // these are test calls to services, showing the authorization context and server side authorization manager.
    //
    // some pass the authorization manager, and some are refused
    // the requests that are refused show toast with the http status code and description
    //
    // put breakpoints in the service project /Security/<YourApp>AuthorizationManager.cs to see 
    // how to use resource authorize attributes to run through the authorization manager for a controller
    //
    /*
    $http.get(appConfig.serviceUri + "Parties");
    $http.get(appConfig.serviceUri + "Parties(1)");
    $http.get(appConfig.serviceUri + "Parties(3)");
    $http.get(appConfig.serviceUri + "Parties(5)");
    $http.get(appConfig.serviceUri + "Cases");
    $http.get(appConfig.serviceUri + "Cases(1)");
    $http.get(appConfig.serviceUri + "Cases(3)");
    $http.get(appConfig.serviceUri + "Cases(5)");
     */
}

angular.module('TS01App')
    .service("peopleService", ["$http", "appConfig", "$q", PeopleService]);