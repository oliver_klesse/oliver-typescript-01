﻿class PeopleServiceMock {
    private testData: ISimpleDataItem[];
    constructor(private $http: ng.IHttpService, private appConfig: Common.IConfig, private $q: ng.IQService) {

    }

    getPeople(): ng.IPromise<ISimpleDataItem[]> {
        var deferred = this.$q.defer();
        this.createTestData();
        deferred.resolve(this.testData);
        return deferred.promise;
    }

    getPerson(id: number): ng.IPromise<ISimpleDataItem> {
        var deferred = this.$q.defer();
        this.createTestData();
        deferred.resolve(this.findTestItem(id));
        return deferred.promise;
    }

    savePerson(item: ISimpleDataItem) {
        // implement http call
    }

    createTestData() {
        if (!this.testData) {
            this.testData = [
                { FirstName: "First", LastName: "Last", Id: 1 },
                { FirstName: "John", LastName: "Doe", Id: 2 }
            ];
        }
    }

    findTestItem(id: number): ISimpleDataItem {
        var item: ISimpleDataItem = null;
        angular.forEach(this.testData, (testItem: ISimpleDataItem) => {
            if (testItem.Id === id) {
                item = testItem;
            }
        });
        return item;
    }

}

angular.module('TS01App')
    .service("peopleService", ["$http", "appConfig", "$q", PeopleServiceMock]);