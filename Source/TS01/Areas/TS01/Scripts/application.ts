﻿angular.module("TS01App",
    ['ngAria', 'ngRoute', 'TylerUI', 'TylerUI.Components', 'gettext', 'ngScrollSpy'])
    .constant('appConfig', (<any>window).appConfig)
    .config([
        '$routeProvider', '$locationProvider', 'appConfig', ($routeProvider, $locationProvider, appConfig) => {
            $routeProvider
                .when('/list', { templateUrl: appConfig.virtualDirectory + 'Areas/TS01/Views/Index/List.html', controller: 'peopleListController', controllerAs: "vm" })
                .when('/edit/:id', { templateUrl: appConfig.virtualDirectory + 'Areas/TS01/Views/Index/Edit.html', controller: 'peopleEditController', controllerAs: "vm" })
                .otherwise({ redirectTo: '/list' });
            $locationProvider.html5Mode(false);
        }
    ])
    // https://docs.angularjs.org/guide/production
    .config([
        '$compileProvider', ($compileProvider: ng.ICompileProvider) => {
            $compileProvider.debugInfoEnabled(false);
        }
    ])
    .run([
        'appConfig', 'ttLocale', 'gettextCatalog', '$rootScope', (
            appConfig: Common.IConfig, ttLocale: Common.ILocale, gettextCatalog: any, $rootScope: ng.IRootScopeService) => {

        //
        ttLocale.init(gettextCatalog);
        ttLocale.setCurrentLanguage(appConfig.acceptLanguages);
        ttLocale.setDebug(false); //for debug, if true, prepend [MISSING]: to each untranslated string.
    }]);

// bootstrap the app
$(angular.element(document).ready(function () {
    angular.bootstrap(angular.element((<any>window).app), ['TS01App'], {
        // https://docs.angularjs.org/guide/di#using-strict-dependency-injection
        strictDi: false
    });
}));

