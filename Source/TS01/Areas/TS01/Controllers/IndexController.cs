﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tyler.WebApp.Security.ActionFilter;

namespace TS01.Areas.TS01.Controllers
{
    [RouteArea("TS01", AreaPrefix = "")]
    public class IndexController : Controller
    {
        [Route("~/")]
        [JwtInViewBag]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            if (Request.QueryString.Count > 0)
            {
                // handle intent url - digest query string and redirect
                //var id = Request.QueryString["id"];
                //var action = Request.QueryString["action"];
                //var hl = Request.QueryString["hl"];

                var spaUrl = Url.Action("Index");
                var bookmark = "#/list";
                var url = spaUrl + bookmark;

                return new RedirectResult(url);
            }
            else
            {
                // redirect to add trailing slash if request is pointing to virtual directory without it
                if (Request.Path == Request.ApplicationPath)
                    return new RedirectResult(Url.Action("Index"), true);
                else
                    return View();
            }
        }

        [Route("~/Manifest")]
        [AllowAnonymous]
        public string Manifest()
        {
            return TylerUI.TylerUIHtml5Cache.BuildManifestResponse(
              Response,
              new TS01CacheModel()
              );
        }
    }
}