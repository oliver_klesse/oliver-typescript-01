﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TS01.Areas.TS01
{
    public class TS01AreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TS01";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}