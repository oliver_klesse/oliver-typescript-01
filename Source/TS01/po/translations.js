angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('es', {"Cancel":"Cancelar","Edit":"Editar","Editing {{item.value}}":"Edición de {{item.value}}","First Name":"Nombre primero","From Date":"Fecha Desde","Last Name":"Apellidos","List of Cards":"Lista de tarjetas","Save":"Guardar","Show Date Picker":"Selector de Fechas","Value":"Valor"});
/* jshint +W100 */
}]);