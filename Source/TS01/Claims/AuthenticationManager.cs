﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using Tyler.WebApp.Security.Web;

namespace TS01.Claims
{
    // http://msdn.microsoft.com/en-us/library/system.security.claims.claimsauthenticationmanager(v=vs.110).aspx
    public class AuthenticationManager : TylerClaimsAuthenticationManager
    {
        protected override ClaimsPrincipal PostProcessClaimsPrincipal(ClaimsPrincipal incomingPrincipal)
        {
            HttpContext.Current.User = incomingPrincipal;
            var claims = new List<Claim>(incomingPrincipal.Claims);

            // remove any un-needed claims here

            // add any custom claims here

            // return a new Claims Principal w/ the modified claims list
            return new ClaimsPrincipal(new ClaimsIdentity(claims, "PostProcessClaimsPrincipal"));
        }

    }



}