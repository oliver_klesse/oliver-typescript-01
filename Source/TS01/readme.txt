## Next Steps ##

## Cleanup

 Close VS2015 and run `0_Cleanup_Temp_Files_That_VS_Had_Locked.bat` to cleanup the solution directories. 
 VS2015 Locks some files during and after project creation, so this allows those to get cleaned up.

### Update NuGet Packages ###
  
  All the missing NuGet packages will be automatically restored when you build your project. 
   
### TylerUI Example Project ###
  
  In addition to the basic app created by the VSIX, the TylerUIExample apps are installed for reference.

  http://localhost/app/TS01/TylerUIExample/Dashboard/Index
     

## Helper Scripts ##

  Powershell scripts located at your web app project root (C:\Enterprise\Sprint-2-Training\TS01)


  ### create_accurev_streams.ps1 ###

  This script will create a workspace and all the necessary streams for your project in the WebApps depot
  in AccuRev. Run this script when you are ready to put your project into AccuRev. Remember that you cannot
  delete streams in AccuRev or reuse stream names. Just make sure you are ready.


  ### create_teamcity_project.ps1 ###

  This script will create a TeamCity project for your web app. It includes the following:
    * Auto-build on check in to the dev stream
    * Auto-publish on successful build
    * Auto-deploy to the Web App build server (pladvsvwebapp)
    * Auto-build on check in to the qa stream
    * Auto-publish to internal QA streams on successful build

  Visit the parent project in TeamCity
    http://teamcity.tylertech.com/project.html?projectId=WebApps




## Additional Documentation ##

  http://pladvsv-tldr

