﻿angular.module('TylerUI').directive('ttKeybinding', [function () {
	return {
		restrict: 'E',
		scope: {
			invoke: '&',
			enabled: "=?",
			preventDefault: "=?"
		},
		link: function (scope, el, attr) {
			// Default to enabled = true (only set false if expression evaluates to such)
			scope.enabled = scope.enabled !== false;
			
			// Default to preventDefault = true (only set false if expression evaluates to such)
			scope.preventDefault = scope.preventDefault !== false;
			
			Mousetrap.bind(attr.on, function(e) {

				if (scope.enabled) {
					scope.invoke();

					// Prevent default keyboard command in browser - don't bubble up
					if (scope.preventDefault) {
						return false;
					}
				}
			}, 'keydown'); // Map to "keydown" - this make the binding case insensitive
			
		}
	};
}]);