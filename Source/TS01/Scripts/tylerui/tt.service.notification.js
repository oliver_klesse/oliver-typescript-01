﻿angular.module('TylerUI').factory('ttNotificationService', [function () {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var copy = $.extend({}, toastr.options);
    return {
        info: function (message, option) {
            if (option && option == 'closeButtonOn') {
                toastr.options.closeButton = true;
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
            }
            toastr.info(message, "Info");
            toastr.options = copy;
        },
        success: function (message, option) {
            if (option && option == 'closeButtonOn') {
                toastr.options.closeButton = true;
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
            }
            toastr.success(message, "Success");
            toastr.options = copy;
        },
        warning: function (message, option) {
            if (option && option == 'closeButtonOn') {
                toastr.options.closeButton = true;
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
            }
            toastr.warning(message, "Warning");
            toastr.options = copy;
        },
        error: function (message, option) {
            if (option && option == 'closeButtonOn') {
                toastr.options.closeButton = true;
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
            }
            toastr.error(message, "Error");
            toastr.options = copy;
        }
    };
}]);

