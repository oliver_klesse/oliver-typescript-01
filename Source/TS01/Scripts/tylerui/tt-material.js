﻿(function () {
  "use strict";

  /**
   * @ngdoc module
   * @name tt-material.components.facet
   *
   * @description
   * Facet components.
   */

  //angular.module('tt-material.components.facet', [
  //  'material.core'
  //])
  //  .directive('ttFacet', ttFacetDirective);



  /**
   * @ngdoc directive
   * @name ttFacet
   * @module tt-material.components.facet
   *
   * @restrict E
   *
   * @description
   * The `<tt-facet>` directive is a container element used within `<md-content>` containers.
   *
   * !!!!!
   * An image included as a direct descendant will fill the card's width, while the `<md-card-content>`
   * container will wrap text content and provide padding. An `<md-card-footer>` element can be
   * optionally included to put content flush against the bottom edge of the card.
   *
   * Action buttons can be included in an element with the `.md-actions` class, also used in `md-dialog`.
   * You can then position buttons using layout attributes.
   *
   * Cards have constant width and variable heights; where the maximum height is limited to what can
   * fit within a single view on a platform, but it can temporarily expand as needed.
   *
   * @usage
   * ###Card with optional footer
   * <hljs lang="html">
   * <tt-facet>
   *  <tt-facet-item-template>
   *    <h2>Card headline</h2>
   *    <p>Card content</p>
   *  </tt-facet-item-template>
   *  <tt-more-template>
   *    Card footer
   *  </tt-more-template>
   * </tt-facet>
   * </hljs>
   *
   */

  //function ttFacet($mdTheming) {
  //  return {
  //    restrict: 'E',
  //    link: function ($scope, $element, $attr) {
  //      $mdTheming($element);
  //    },
  //    template: function (element, attr) {
  //      var noItemsTemplate = getNoItemsTemplate(),
  //          itemTemplate = getItemTemplate(),
  //          leftover = element.html();
  //      return '\
  //        <tt-facet-wrap\
  //            layout="row"\
  //            role="listbox">\
  //          <ul role="presentation"\
  //              id="ul-{{$ttFacetCtrl.id}}"\
  //              ng-hide="$ttFacetCtrl.hidden"\
  //              ng-mouseenter="$ttFacetCtrl.listEnter()"\
  //              ng-mouseleave="$ttFacetCtrl.listLeave()"\
  //              ng-mouseup="$ttFacetCtrl.mouseUp()">\
  //            <li ng-repeat="(index, item) in $ttFacetCtrl.buckets"\
  //                ng-class="{ selected: index === $ttFacetCtrl.index }"\
  //                ng-click="$ttFacetCtrl.select(index)"\
  //                tt-facet-list-item="$ttFacetCtrl.itemName">\
  //                ' + itemTemplate + '\
  //            </li>\
  //            ' + noItemsTemplate + '\
  //          </ul>\
  //        </tt-facet-wrap>\
  //        <aria-status\
  //            class="md-visually-hidden"\
  //            role="status"\
  //            aria-live="assertive">\
  //          <p ng-repeat="message in $ttFacetCtrl.messages" ng-if="message">{{message}}</p>\
  //        </aria-status>';

  //      function getItemTemplate() {
  //        var templateTag = element.find('md-item-template').remove(),
  //            html = templateTag.length ? templateTag.html() : element.html();
  //        if (!templateTag.length) {
  //          element.empty();
  //        }
  //        return html;
  //      }

  //      function getNoItemsTemplate() {
  //        var templateTag = element.find('md-not-found').remove(),
  //            html = templateTag.length ? templateTag.html() : '';
  //        return html
  //            ? '<li ng-if="!$ttFacetCtrl.buckets.length && !$ttFacetCtrl.loading\
  //              && !$ttFacetCtrl.hidden"\
  //              ng-hide="$ttFacetCtrl.hidden"\
  //              tt-facet-parent-scope>' + html + '</li>'
  //            : '';
  //      }


  //    }
  //  };
  //}
  //ttFacet.$inject = ["$mdTheming"];

})();