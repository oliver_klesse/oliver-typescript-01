﻿/* used by tt-busy-indicator */
angular.module("TylerUI").config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
    var requests = 0;
    function fireStartEvent() {
      if (!requests) {
        $rootScope.$broadcast("ajax-start");
      }
      requests++;
    }
    function fireStopEvent() {
      requests--;
      if (!requests) {
        $rootScope.$broadcast("ajax-stop");
      }
    }
    return {
      'request': function (config) {
        fireStartEvent();
        return $q.when(config);
      }, 'response': function (response) {
        fireStopEvent();
        return $q.when(response);
      }, 'responseError': function (rejection) {
        fireStopEvent();
        return $q.reject(rejection);
      }
    };
  }]);
}]);

angular.module("TylerUI").directive("ttBusyIndicator", [function () {
  return {
    restrict: 'E',
    replace: 'true',
    template: '<div class="tt-busy-indicator"><div class="icon"><div class="rect rect1"></div> <div class="rect rect2"></div><div class="rect rect3"></div></div>',
    scope: {
      show: '=',
      inline: '@'
    },
    link: function (scope, element, attrs) {
      if (attrs.show === undefined) {
        scope.$on("ajax-start", function () {
          element.show();
        });
        scope.$on("ajax-stop", function () {
          element.hide();
        });
      } else {
        scope.$watch('show', function (value) {
          if (value) {
            element.show();
          } else {
            element.hide();
          }
        });
      }
      var iconElement = angular.element(element[0].firstChild);
      if (attrs.size !== undefined) {
        //iconElement.attr('style', 'height:' + attrs.size + ';width:' + attrs.size + ';');
        iconElement.attr('style', 'height:' + attrs.size + ';');
        element.addClass('tt-busy-indicator-inline');
      } else {
        iconElement.removeAttr('style');
        element.removeClass('tt-busy-indicator-inline');
      }
    }
  };
}]);

