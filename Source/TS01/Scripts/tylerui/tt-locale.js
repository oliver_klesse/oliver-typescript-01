﻿angular.module('TylerUI').factory('ttLocale', ['gettextCatalog', function (gettextCatalog) {
    return {
        //fixme - why is gettextCatalog not injecting into this module properly?
        init: function(gettextCatalog) {
            this.catalog = gettextCatalog;
        },
        setCurrentLanguage: function (acceptLanguages) {
            // wrapped so can cascade language selection from specific to general
            // es_ES > es > en
            this.catalog.setCurrentLanguage(acceptLanguages[0].split('-')[0]);
        },
        setDebug: function (isDebug) {
            this.catalog.debug = isDebug;
        },
        getString: function (message, parameters) {
            return this.catalog.getString(message, parameters);
        }
        //fixme add angular $locale helpers for date and numbers
        //fixme add pluralizer 
        //https://angular-gettext.rocketeer.be/dev-guide/
    }
}]);

