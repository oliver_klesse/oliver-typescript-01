﻿// http://craig.is/killing/mice
// see IV.Mousetrap.stopCallback section @ above link

// Save off the default stopCallback function
var defaultStopCallback = Mousetrap.stopCallback;

Mousetrap.stopCallback = function (e, element, combo) {

	if (combo === "esc") { // Allow some combo keys to work on all input fields, Esc for example (go ahead and execute the keybinding)
		return false;
	}
	
	// Call default Mousetrap function
	return defaultStopCallback(e, element, combo);
}