﻿angular.module('omniBarApp', ['ngAria', 'ngMaterial'])
  .constant('appConfig', window.omniBarConfig)
  .controller('omniCtrl', ['$scope', 'appConfig', '$log', '$window', function ($scope, appConfig, $log, $window) {
      // baseUri should eventually come from the app service based on installed manifests and / or launch context/url
    //var baseUri = "";
    var baseUri = appConfig.virtualDirectory,
        phAppIcon = baseUri + "Content/tylerui/svgs/ph_app.svg",
        tylerLogo = baseUri + "Content/tylerui/svgs/Icon_Talking_Ts.svg"

    function endsWith(str, suffix) {
      return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    $scope.curApp = {
      name: appConfig.appname
    }

    $scope.hasValue = function (value) {
      return !!value && value !== '';
    };

    $scope.signout = function () {
        $window.location.assign(appConfig.virtualDirectory + "signout");
    };

    $scope.omnibar = [
      {
        "type": "apps",
        // fixme query intents service
        "apps": [
          { "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Web App", "url": baseUri + "TylerUIExample/SinglePageApp/" },
          //{ "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Steps", "url": baseUri + "TylerUIExample/Steps/" },
          //{ "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Components", "url": baseUri + "TylerUIExample/Components/" },
          //{ "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Formly", "url": baseUri + "TylerUIExample/Formly/" },
          //{ "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Timeline", "url": baseUri + "TylerUIExample/Recorder/" },
          { "iconSrc": baseUri + "Areas/TylerUIExample/Content/images/icon.png", "name": "Search", "url": baseUri + "TylerUIExample/ElasticSearch/" }
        ]
      },
      //{
      //  "type": "notifications",
      //  "label": "My Notifications",
      //  "notifications": [
      //    //{ "userProfileSrc": baseUri + "Content/tylerui/images/placeholderprofile.png", "userName": "User Name", "msg": "message", "userMsg": "user message" },
      //    //{ "userProfileSrc": baseUri + "Content/tylerui/images/placeholderprofile.png", "userName": "User Name", "msg": "message", "userMsg": "user message" },
      //    //{ "userProfileSrc": baseUri + "Content/tylerui/images/placeholderprofile.png", "userName": "User Name", "msg": "message", "userMsg": "user message" },
      //    //{ "userProfileSrc": baseUri + "Content/tylerui/images/placeholderprofile.png", "userName": "User Name", "msg": "message", "userMsg": "user message" }
      //  ]
      //},
      {
        "type": "userLinks",
        "userLinks": [
          //{ "url": "someurl", "urlname": "Profile" },
          //{ "url": "someurl", "urlname": "History" },
          //{ "url": "someurl", "urlname": "Settings" },
          { "url": baseUri + "Signout", "urlname": "Logout" }
        ]
      }
    ];

    $scope.getBrand = function () {
      //add logic to see if client has a logo.
      return tylerLogo;
    };

    $scope.getIcon = function (src) {
      if ($scope.hasValue(src)) {
        if (endsWith(src, '.svg')) {
          return src;
        } else {
          //$log.log('md-svg-src requires a svg format, got:', src);
          return phAppIcon;
        }
      } else {
        //$log.log('md-svg-src needs a valid source, got:', src);
        return phAppIcon;
      }
    }
  }]);

// manually start the Omni bar app so we can still use ng-app for the main application
$(angular.element(document).ready(function () {
  angular.bootstrap(angular.element(omnibar), ['omniBarApp']);
}));