﻿'use strict';

/*
A simple AngularJS directive to render a smooth scroll effect
Usage: <element smooth-scroll target='id' [offset='value']></element>
@author: Arnaud BRETON (arnaud@videonot.es)
Inspired by http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
*/

/*
 * Tyler Update
 * Modified from directive to factory for use inside of other factories. Example: ttFocus
 * 
 * Now uses Element instead of Element.id
 */

angular.module('TylerUI.Components')
  .factory('ttSmoothScroll', ['$log', '$timeout', '$window', function ($log, $timeout, $window) {
    /*
        Retrieve the current vertical position
        @returns Current vertical position
    */

    var currentYPosition, elemYPosition, smoothScroll;
    currentYPosition = function () {
      if ($window.pageYOffset) {
        return $window.pageYOffset;
      }
      if ($window.document.documentElement && $window.document.documentElement.scrollTop) {
        return $window.document.documentElement.scrollTop;
      }
      if ($window.document.body.scrollTop) {
        return $window.document.body.scrollTop;
      }
      return 0;
    };
    /*
        Get the vertical position of a DOM element
        @param DOM element
        @returns The vertical position of DOM element
    */

    elemYPosition = function (elem) {
      var node, y;
      if (elem) {
        y = elem.offsetTop;
        node = elem;
        while (node.offsetParent && node.offsetParent !== document.body) {
          node = node.offsetParent;
          y += node.offsetTop;
        }
        return y;
      }
      return 0;
    };
    /*
        Smooth scroll to element without offset
        @param The element to scroll to
        @param offSet Scrolling offset
    */

    smoothScroll = function (elem, offSet) {
      var distance, i, leapY, speed, startY, step, stopY, timer, _results;
      startY = currentYPosition();
      stopY = elemYPosition(elem) - offSet;
      distance = (stopY > startY ? stopY - startY : startY - stopY);
      if (distance < 100) {
        scrollTo(0, stopY);
        return;
      }
      speed = Math.round(distance / 100);
      if (speed >= 20) {
        speed = 20;
      }
      step = Math.round(distance / 25);
      leapY = (stopY > startY ? startY + step : startY - step);
      timer = 0;
      if (stopY > startY) {
        i = startY;
        while (i < stopY) {
          setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
          leapY += step;
          if (leapY > stopY) {
            leapY = stopY;
          }
          timer++;
          i += step;
        }
        return;
      }
      i = startY;
      _results = [];
      while (i > stopY) {
        setTimeout('window.scrollTo(0, ' + leapY + ')', timer * speed);
        leapY -= step;
        if (leapY < stopY) {
          leapY = stopY;
        }
        timer++;
        _results.push(i -= step);
      }
      return _results;
    };
    return function (elem, offsetValue) {
      var offset;
      if (elem) {
        offset = offsetValue || 100;
        //$log.log('Smooth scroll: scrolling to', elem, 'with offset', offset);
        return smoothScroll(elem, offset);
      } else {
        return $log.warn('Smooth scroll: no target specified');
      }
    };
  }]);