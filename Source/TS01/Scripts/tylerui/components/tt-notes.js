﻿// Notes for Action Panels
angular.module('TylerUI.Components')
.directive('ttNotes', ['appConfig', function (appConfig) {
  function updateNotes($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.hasValue($scope.template)) {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-notes.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-notes.html';
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    scope: {
      notes: '=',
      template: '=?',
      itemTemplate: '=?',
      startAddCommand: '=',
      moreCommand: '=',
      displayThreshold: '=?'
    },
    controller: ['$scope', function ($scope) {
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };
      $scope.displayThreshold = $scope.displayThreshold || 3;
      $scope.itemTemplate = $scope.hasValue($scope.itemTemplate)
        ? $scope.itemTemplate
        : appConfig.virtualDirectory === undefined
          ? $scope.resolvedItemTemplate = '/Scripts/tylerui/components/tt-note-card.html'
          : $scope.resolvedItemTemplate = appConfig.virtualDirectory + '/Scripts/tylerui/components/tt-note-card.html';
    }],
    link: function (scope, element, attrs) {
      updateNotes(scope, element, attrs);
    }
  }
}])

// Notes for full screen
.directive('ttNotesFull', ['appConfig', function (appConfig) {
  function updateNotes($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.hasValue($scope.template)) {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-notes-full.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-notes-full.html';
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    scope: {
      notes: '=',
      template: '=?',
      itemTemplate: '=?',
      cancelCommand: '='
    },
    controller: ['$scope', function ($scope) {
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };
      $scope.itemTemplate = $scope.hasValue($scope.itemTemplate)
        ? $scope.itemTemplate
        : appConfig.virtualDirectory === undefined
          ? $scope.resolvedItemTemplate = '/Scripts/tylerui/components/tt-note-card.html'
          : $scope.resolvedItemTemplate = appConfig.virtualDirectory + '/Scripts/tylerui/components/tt-note-card.html';
    }],
    link: function (scope, element, attrs) {
      updateNotes(scope, element, attrs);
    }
  }
}])

.directive('ttAddNote', ['appConfig', function (appConfig) {
  function updateComment($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.hasValue($scope.template)) {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-add-note.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-add-note.html';
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    scope: {
      user: '=',
      notes: '=',
      template: '=?',
      submitCommand: '=',
      cancelCommand: '=',
    },
    controller: ['$scope', function ($scope) {
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };
      $scope.text = '';
      $scope.reset = function () {
        $scope.text = '';
      };
    }],
    link: function (scope, element, attrs) {
      updateComment(scope, element, attrs);
    }
  }
}]);