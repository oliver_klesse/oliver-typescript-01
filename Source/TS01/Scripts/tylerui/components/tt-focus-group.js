﻿angular.module('TylerUI.Components')
.directive('ttFocusGroup', ['ttFocusManager', 'focus', '$timeout', function (ttFocusManager, focus, $timeout) {
  var groupCtrl = ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
    // private
    var containers = [],
        cyclic = false,
        id = $element.id,
        identifier = '[tt-focus-group]',
        isEnd = false,
        isStart = false,
        onEndOfGroup = null,
        onStartOfGroup = null,
        retainSelected = false,
        selectedContainer = null,
        selectedItem = null;
    function hasValue(value) {
      return !!value && value !== '';
    }
    function isSelectedInItems(selected, items) {
      var bool = hasValue(selected);
      if (bool) {
        var count = items.length;
        for (var i = 0; i < count; i++) {
          var item = items[i];
          if (selected === item) {
            return true;
            break;
          }
        }
      }
      return false;
    }
    function updateBounds() {
      var i = containers.indexOf(selectedContainer);
      var j = selectedContainer.controller.items.indexOf(selectedItem);

      var containerCount = containers.length - 1;
      var isLastContainer = i >= containerCount;
      var itemCount = selectedContainer.controller.items.length - 1;
      var isLastItem = j >= itemCount;
      isEnd = isLastContainer && isLastItem;

      var isFirstContainer = i <= 0;
      var isFirstItem = j <= 0;
      isStart = isFirstContainer && isFirstItem;
    }

    // public
    this.element = $element;
    this.isPrimary = false;

    this.clear = function () {
      if (!retainSelected && !this.isPrimary) {
        selectedContainer.controller.clear();
        selectedContainer = null;
        selectedItem = null;
      }
    }
    this.getID = function () {
      return id;
    }
    this.goToSelected = function () {
      if (!hasValue(selectedContainer)) {
        selectedContainer = containers[0];
      }
      selectedItem = selectedContainer.controller.getSelectedItem();
      if (!hasValue(selectedItem)) {
        selectedContainer.controller.first();
        selectedItem = selectedContainer.controller.getSelectedItem();
      }
      focus(selectedItem.element);
      updateBounds();
    }
    this.initialize = function (options) {
      this.element = options.elem;
      id = options.elem.id;
      if (hasValue(options.attrs.ttOnEndOfGroup)) {
        onEndOfGroup = $scope.$eval(options.attrs.ttOnEndOfGroup);
      }
      if (hasValue(options.attrs.ttOnStartOfGroup)) {
        onStartOfGroup = $scope.$eval(options.attrs.ttOnStartOfGroup);
      }
      cyclic = angular.isDefined(options.attrs.ttCyclic);
      retainSelected = angular.isDefined(options.attrs.ttRetainSelected);
      this.isPrimary = angular.isDefined(options.attrs.ttPrimaryGroup);
    }
    this.next = function () {
      if (hasValue(selectedContainer)) {
        // make sure we have the current item
        selectedItem = selectedContainer.controller.getSelectedItem();
        if (!hasValue(selectedItem)) {
          selectedContainer.controller.first();
        }

        if (selectedContainer.controller.isEnd) {
          // end of container event
          selectedContainer.controller.onEndOfContainer();
          selectedContainer.controller.updateBounds();
        }

        if (!selectedContainer.controller.isEnd) {
          // next item
          selectedContainer.controller.next();
        } else if (containers.length > 1) {
          // or next container, first item
          var nextIndex = isEnd ? containers.indexOf(selectedContainer) : containers.indexOf(selectedContainer) + 1;
          var nextContainer = containers[nextIndex];

          if (selectedContainer !== nextContainer) {
            selectedContainer.controller.clear();
            selectedContainer = nextContainer;
            selectedContainer.controller.first();
          }
        }

        if (isEnd) {
          if (hasValue(onEndOfGroup)) {
            // end of the group event
            onEndOfGroup();
          }

          if (cyclic) {
            // cycle to the beginning and get first container, first item
            selectedContainer.controller.clear();
            selectedContainer = containers[0];
            selectedContainer.controller.first();
          }
        }
      } else {
        // we lost our selected container so lets set it to the first one.
        selectedContainer = containers[0];
        selectedContainer.controller.first();
      }
      // focus the new item and update our validation.
      selectedItem = selectedContainer.controller.getSelectedItem();
      focus(selectedItem.element);
      updateBounds();
    }
    this.onContainerClicked = function (containerController) {
      if (hasValue(containerController)) {
        if (hasValue(selectedContainer)) {
          if (containerController !== selectedContainer.controller) {
            selectedContainer.controller.clear();
          }
        }
        var count = containers.length;
        for (var i = 0; i < count; i++) {
          var container = containers[i];
          if (container.controller === containerController) {
            selectedContainer = container;
            break;
          }
        }
        selectedItem = selectedContainer.controller.getSelectedItem();
        ttFocusManager.setSelectedGroup(id);
      }
    }
    this.prev = function () {
      if (hasValue(selectedContainer)) {
        // make sure we have the current item
        selectedItem = selectedContainer.controller.getSelectedItem();
        if (!hasValue(selectedItem)) {
          selectedContainer.controller.first();
        }

        if (selectedContainer.controller.isStart) {
          // start of container event
          selectedContainer.controller.onStartOfContainer();
          selectedContainer.controller.updateBounds();
        }

        if (!selectedContainer.controller.isStart) {
          // prev item
          selectedContainer.controller.prev();
        } else if (containers.length > 1) {
          // or prev container, last item
          var prevIndex = isStart ? containers.indexOf(selectedContainer) : containers.indexOf(selectedContainer) - 1;
          var prevContainer = containers[prevIndex];

          if (selectedContainer !== prevContainer) {
            selectedContainer.controller.clear();
            selectedContainer = prevContainer;
            selectedContainer.controller.last();
          }
        }

        if (isStart) {
          if (hasValue(onStartOfGroup)) {
            // start of the group event
            onStartOfGroup();
          }

          if (cyclic) {
            // cycle to the end and get last container, last item
            selectedContainer.controller.clear();
            selectedContainer = containers[containers.length - 1];
            selectedContainer.controller.last();
          }
        }
      } else {
        // we lost our selected container so lets ste it to the first one.
        selectedContainer = containers[0];
        selectedContainer.controller.first();
      }
      // focus the new item and update our validation
      selectedItem = selectedContainer.controller.getSelectedItem();
      focus(selectedItem.element);
      updateBounds();
    }
    this.registerContainer = function (containerController, order) {
      var newContainer,
          containerCount;
      newContainer = { controller: containerController, order: order || 0 };
      containerCount = containers.length;
      if (containerCount > 0) {
        for (var i = 0; i < containerCount; i++) {
          if (newContainer.order < containers[i].order) {
            containers.splice(i, 0, newContainer);
          } else if (i == containerCount - 1) {
            containers.push(newContainer);
          }
        }
      } else {
        containers.push(newContainer);
      }
    }
    this.unregisterContainer = function (containerController) {
      var count = containers.length;
      for (var i = 0; i < count; i++) {
        var container = containers[i];
        if (container.controller.element === containerController.element) {
          containers.splice(i, 1);
          break;
        }
      }
      if (containers.length === 0) {
        selectedContainer = null;
        selectedItem = null;
      }
    }
  }];

  return {
    controller: groupCtrl,
    require: ['ttFocusGroup'],
    link: function ($scope, elem, attrs, controllers) {
      var ttFocusGroup = controllers[0];

      ttFocusGroup.initialize({
        identifier: '[tt-focus-group]',
        elem: elem[0],
        attrs: attrs
      });

      // register group
      $timeout(function () {
        ttFocusManager.registerGroup(ttFocusGroup);
      });

      // unregister group
      $scope.$on('$destroy', function () {
        ttFocusManager.unregisterGroup(ttFocusGroup);
      })
    }
  };
}]);