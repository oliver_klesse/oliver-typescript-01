﻿angular.module('TylerUI.Components')
.service('ttStepsService', ['$location', function ($location) {
  return {
    steps: [],
    index: 0,
    isInitialized : false,
    isVisible: true,
    visibility: function (bool) {
      this.isVisible = bool;
    },
    to: function (i) {
      this.index = i;
    },
    next: function (gotoRoute) {
      if (this.index < this.steps.length - 1) {
        this.steps[this.index].isDisabled = false; //enable previous step nav button
        this.steps[this.index].route = $location.url(); // save current route
        this.index += 1;
        this.steps[this.index].route = gotoRoute != null ? gotoRoute : this.steps[this.index].route;
        this.steps[this.index].isDisabled = true; //disable active step nav button.
      }
    },
    prev: function (gotoRoute) {
      if (this.index > 0) {
        this.steps[this.index].isDisabled = false; //enable next step nav button since we have visited that step
        this.steps[this.index].route = $location.url(); // save the current route
        this.index -= 1;
        this.steps[this.index].route = gotoRoute != null ? gotoRoute : this.steps[this.index].route;
        this.steps[this.index].isDisabled = true; //disable active step nav button.
      }
    },
    disableAfterIndex: function (curIndex) {
      var stepsLength = this.steps.length;
      for (i = curIndex + 1; i < stepsLength; ++i) {
        this.steps[i].isDisabled = true;
      }
    },
    disable: function (i) {
      this.steps[i].isDisabled = true;
    },
    enable: function (i) {
      this.steps[i].isDisabled = false;
    }
  }
}])

.directive('ttSteps', ['$location', 'ttStepsService', function ($location, ttStepsService) {
  function viewStep(index) {
    ttStepsService.steps[ttStepsService.index].isDisabled = false; //enable next step nav button since we have visited that step
    var stepsLength = ttStepsService.steps.length;
    if (stepsLength > 0) {
      for (var i = 0; i < stepsLength; ++i) {
        ttStepsService.steps[i].activeClass = '';
      }
      ttStepsService.steps[index].activeClass = 'active';
      ttStepsService.steps[index].isDisabled = true; //disable active step nav button.
      ttStepsService.index = index;
      if (ttStepsService.isInitialized) { // do not navigate on initial load
        $location.path(ttStepsService.steps[index].route);
      } else {
        ttStepsService.isInitialized = true;
      }
    }
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    scope: {},
    controller: function ($scope) {
      $scope.select = function (index) {
        var navigate = true;
        if (typeof (ttStepsService.steps[index].canNavigate) == "function") {
          if (!ttStepsService.steps[index].canNavigate()) {
            navigate = false;
          }
        }
        if (navigate === true) {
          viewStep(index);
        }
      };
    },
    link: function (scope, element, attrs) {
      scope.steps = ttStepsService.steps;
      scope.$watch(
        function () { return ttStepsService.index },

        function (newValue) {
          scope.index = newValue;
          viewStep(scope.index);
        });

      scope.getContentUrl = function () {
        // include the (optional) virtualdirectory path
        if (attrs.virtualdirectory === undefined) {
          return '/Scripts/tylerui/ttsteps/tt-steps.html';
        } else {
          return attrs.virtualdirectory + 'Scripts/tylerui/ttsteps/tt-steps.html';
        }
      };
    },

  };
}]);