angular.module('TylerUI.Components')
.service('ttTimelineSvc', [function () {
  var isAfterDate,  // is date_A after date_B
      isBeforeDate; // is date_A before date_B

  isAfterDate = function (a, b) {
    var diff = (a - b);
    return diff > 0;
  };

  isBeforeDate = function (a, b) {
    var diff = (b - a);
    return diff > 0;
  };

  return {
    endDate: null,    // end of the timeline
    startDate: null,  // start of the timeline
    hours: [],      // total hours between start and end

    // get position along the timeline between startDate and endDate
    getPosition: function (positionDate) {
      var diff,     // length of between startDate and positionDate
          isAfter,  // is positionDate after startDate
          isBefore, // is positionDate before endDate
          length,   // length of the timeline in milliseconds

      isAfter = isAfterDate(positionDate, this.startDate);
      isBefore = isBeforeDate(positionDate, this.endDate);
      length = (this.endDate - this.startDate);

      if (isAfter && isBefore) {
        diff = (positionDate - this.startDate);
        return (diff / length);
      } else {
        // positionDate does not exist within the range of the timeline
        return null;
      }
    },

    // converts a normal (0.0 to 1.0) to a percent (0.0 - 100.0)
    normalToPercent: function (value) {
      return (100 * value);
    },

    // sets the selected event, passed into the directive.
    setSelectedEvent: null,

    // create hour markers for every hour on the timeline
    setHours: function () {
      var count,
          index;

      count = this.endDate.getHours() - this.startDate.getHours();

      //skip the startDate hour by setting index to 1
      for (index = 1; index < count; index++) {
        //skip the noon hour
        if (this.startDate.getHours() + index !== 12) {
          var hour = new Date(
          this.startDate.getFullYear(),
          this.startDate.getMonth(),
          this.startDate.getDate(),
          this.startDate.getHours() + index,
          0,
          0,
          0);

          this.hours.push(hour);
        }
      }
    }
  }
}])

.directive('ttTimeline', ['ttTimelineSvc', function (ttTimelineSvc) {
  function link(scope, element, attrs) {
    scope.getContentUrl = function () {
      if (scope.template != null && scope.template != undefined && scope.template != '') {
        return scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-timeline.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-timeline.html';
      }
    };

    ttTimelineSvc.startDate = scope.startDate;
    ttTimelineSvc.endDate = scope.endDate;
    ttTimelineSvc.setHours();
    scope.hours = ttTimelineSvc.hours;
  }

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    scope: {
      template: '=?',
      startDate: '=',
      endDate: '=',
      events: '=',
      selectedEvent: '=?',
      setSelectedEvent: '=?'
    },
    link: link
  };
}])

.directive('ttTimelineEventMarker', ['ttTimelineSvc', function (ttTimelineSvc) {
  function link(scope, element, attr) {
    var endPerc,    // Ending percent along length of timeline
        endPos,     // Ending normal along length of timeline
        rangePerc,  // Length between start and end of event
        startPerc,  // Starting percent along length of timeline
        startPos;   // Starting normal along length of timeline

    endPos = ttTimelineSvc.getPosition(scope.eventEndDate);
    endPerc = ttTimelineSvc.normalToPercent(endPos);
    startPos = ttTimelineSvc.getPosition(scope.eventStartDate);
    startPerc = ttTimelineSvc.normalToPercent(startPos);
    rangePerc = endPerc - startPerc;

    element.css({ 'left': startPerc + '%', 'width': rangePerc + '%' });
  }
  return {
    restrict: 'A',
    link: link,
    scope: {
      eventStartDate: '=',
      eventEndDate: '='
    }
  };
}])

.directive('ttHourMarker', ['ttTimelineSvc', function (ttTimelineSvc) {
  function link(scope, element, attr) {
    var hourPerc, // 12:00pm percent along length of timeline
        hourPos;  // 12:00pm normal along length of timeline    

    hourPos = ttTimelineSvc.getPosition(scope.hourDate);
    hourPerc = ttTimelineSvc.normalToPercent(hourPos);

    element.css({ 'left': hourPerc + '%' });
  }
  return {
    restrict: 'A',
    link: link,
    scope: {
      hourDate: '='
    }
  };
}])

.directive('ttNoonMarker', ['ttTimelineSvc', function (ttTimelineSvc) {
  function link(scope, element, attr) {
    var noonPerc, // 12:00pm percent along length of timeline
        noonPos;  // 12:00pm normal along length of timeline

    noonPos = ttTimelineSvc.getPosition(scope.noonDate);
    noonPerc = ttTimelineSvc.normalToPercent(noonPos);

    element.css({ 'left': noonPerc + '%' });
  }
  return {
    restrict: 'A',
    link: link,
    controller: ['$scope', function ($scope) {
      $scope.noonDate = new Date(ttTimelineSvc.startDate.getFullYear(), ttTimelineSvc.startDate.getMonth(), ttTimelineSvc.startDate.getDate(), 12, 0, 0, 0);;
    }],
  };
}]);