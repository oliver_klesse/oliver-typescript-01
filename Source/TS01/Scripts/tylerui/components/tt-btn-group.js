﻿// Modified from the original directive.
// digitalfondue / df-tab-menu
// https://github.com/digitalfondue/df-tab-menu

angular.module('TylerUI.Components')

  /* 
  // Example Usage

  // --------------------------------------------------------------- // 
  // tt-btn-group

  // html
  <tt-btn-group args="{'caseID': case.id}" 
                flex-align="end start"
                click-event="intentEvent"
                fields="[{name:'label'}]"
                more-label="More"
                items="case.webIntents"></tt-btn-group>

  
  // angular controller
  $scope.case = {};
  $scope.case.id = "CR-15-0001";
  $scope.case.webIntents = [{"label": "Cases"}, {"label": "Warrants"}]

  $scope.intentEvent = function(intent, args) {
    // Logic
    $log.log(intent + ' ' + args.caseID);
  }  

  // FYI
  // fields="[{name:'label'}]" = case.webIntents[].label - in this example
  // item-template <button ng-click="clickEvent(item, args)">{{ item[fieldName] }}</button>

  // --------------------------------------------------------------- //
  */

.directive('ttBtnGroup', ['$window', '$timeout', 'appConfig', function ($window, $timeout, appConfig) {
  function updateItems($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.template != null && $scope.template != undefined && $scope.template != '') {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-btn-group.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-btn-group.html';
      }
    };
    var root = $element[0];
    var doc = $window.document;
    var wdw = angular.element($window);
    $scope.dropdownOpen = false;

    var getElementsSize = function () {
      var elements = root.querySelectorAll('[button-item]');
      angular.element(elements).removeClass('ng-hide');
      var elementsSize = [];
      for (var e = 0; e < elements.length; e++) {
        var size = 0;
        if ($scope.hasValue(elements[e])) {
          size = elements[e].offsetWidth;
        }
        if (size > 0) {
          elementsSize[e] = size;
        }
      };
      return elementsSize;
    }

    var getMoreElementSize = function () {
      angular.element(root.querySelector('[more-item]')).removeClass('ng-hide');
      var size = 0;
      var moreItem = root.querySelector('[more-item]');
      if ($scope.hasValue(moreItem)) {
        size = moreItem.offsetWidth;
      }
      return size;
    }

    var getVisibleItems = function (_maxWidth) {
      var visibleItems = [];
      var elementsSize = getElementsSize();
      // we dont really need the scroll bar tolerance since we only use the global scrollbar.
      var sum = getMoreElementSize();

      //40px: scrollbar tolerance. Not proud of this, but it works...
      //var sum = getMoreElementSize() + 40;

      var items = root.querySelectorAll('[button-item]');
      for (var i = 0; i < items.length; i++) {
        sum += elementsSize[i];
        if (sum > _maxWidth) {
          return visibleItems;
        } else {
          visibleItems.push(i);
        }
      }
      return visibleItems;
    };

    var buildGroup = function () {
      var btnContainer = root.querySelector('[button-container]');
      var maxWidth = 0;
      if ($scope.hasValue(btnContainer)) {
        maxWidth = btnContainer.offsetWidth;
      }
      var visibleItems = getVisibleItems(maxWidth);
      var elements = root.querySelectorAll('[button-item]');
      var elementCount = elements.length;
      var moreElements = root.querySelectorAll('[dropdown-item]');
      var moreMenuToggle = root.querySelector('[more-item]');

      if (visibleItems.length < elementCount) {
        angular.element(moreMenuToggle).removeClass('ng-hide').attr('aria-hidden', 'false');

        for (var i = 0; i < elementCount; i++) {
          if (visibleItems.indexOf(i) != -1) {
            $scope.items[i].isExpanded = true;
            angular.element(elements[i]).removeClass('ng-hide').attr('aria-hidden', 'false');
          } else {
            $scope.items[i].isExpanded = false;
            angular.element(elements[i]).addClass('ng-hide').attr('aria-hidden', 'true');
          }
        }
      } else {
        for (var i = 0; i < elementCount; i++) {
          $scope.items[i].isExpanded = true;
        }
        angular.element(moreMenuToggle).addClass('ng-hide').attr('aria-hidden', 'true');

        angular.element(elements).removeClass('ng-hide').attr('aria-hidden', 'false');

        $scope.dropdownOpen = false;
        drawDropDown();
      }
      angular.element(elements[elementCount - 1]).addClass('last-child');
    };

    var closeDropdown = function (e) {
      $scope.dropdownOpen = false;
      drawDropDown(e);
    };

    var drawDropDown = function () {
      if ($scope.dropdownOpen) {
        angular.element(root.querySelector('[more-item] [dropdown-toggle]')).addClass('open').attr({ 'aria-expanded': 'true' });
        angular.element(doc).bind('click', closeDropdown);
      } else {
        angular.element(root.querySelector('[more-item] [dropdown-toggle]')).removeClass('open').attr({ 'aria-expanded': 'false' });;
        angular.element(doc).unbind('click', closeDropdown);
      }
    };

    //dropdown controls
    var toggleDropdown = function (e) {
      if (e) { e.stopPropagation() };
      $scope.dropdownOpen = !$scope.dropdownOpen;
      drawDropDown();
    };

    angular.element(root.querySelector('[more-item] [dropdown-toggle]')).bind('click', toggleDropdown);

    wdw.bind('resize', buildGroup);

    $scope.$on('$destroy', function () {
      wdw.unbind('resize', buildGroup);
      angular.element(root.querySelector('[more-item] [dropdown-toggle]')).unbind('click', toggleDropdown);
      angular.element(doc).unbind('click', closeDropdown);
    });

    var buildGroupTimeout;
    $scope.$watch(function () {
      $timeout.cancel(buildGroupTimeout);
      buildGroupTimeout = $timeout(function () {
        buildGroup();
      }, 25, false);
    });

    $attrs.$observe('isCompact', function () {
      $scope.isCompact = $scope.$eval($attrs.isCompact);
      if ($scope.isCompact === undefined) {
        $scope.isCompact = false;
      }
    });
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    controller: ['$scope', '$log', 'appConfig', function ($scope, $log, appConfig) {
      $scope.virtualDirectory = appConfig.virtualDirectory;
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      }

      $scope.toggleClicked = function () {
        console.log('toggle clicked');
      };

      function defaultClickEvent() {
        $log.error("tt-btn-group 'click-event' attribute is unassigned");
      };

      $scope.clickEvent = $scope.clickEvent || defaultClickEvent;

      $scope.dropdownOpen = false;
    }],
    scope: {
      args: '=?',       // additional data.
      clickEvent: '=',  // click event handler
      fields: '=?',
      // the field to bind in each button.  
      // Ex: <tt-btn-group fields="[{name: label}]"/> 
      // {{ item[field.name] }} turns into {{ item.label }}

      flexAlign: '@',   // maps to layout-align from angular material
      items: '=',       // the collection to generate buttons
      moreLabel: '@',   // the display value for the more button
      template: '=?'    // url to override the default html template
    },
    link: function (scope, element, attrs) {
      updateItems(scope, element, attrs);
    }
  }
}]);