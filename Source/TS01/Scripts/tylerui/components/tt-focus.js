﻿angular.module('TylerUI.Components')
.factory('focus', ['$timeout', 'ttSmoothScroll', function ($timeout, ttSmoothScroll) {
  return function (element) {
    // timeout makes sure that is invoked after any other event has been triggered.
    // e.g. click events that need to run before the focus or
    // inputs elements that are in a disabled state but are enabled when those events
    // are triggered.
    $timeout(function () {
      if (element) {
        ttSmoothScroll(element);
        element.focus();
      }
    });
  };
}]);