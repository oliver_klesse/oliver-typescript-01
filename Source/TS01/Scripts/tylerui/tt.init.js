﻿// iniitalize the TylerUI module
angular.module('TylerUI', ['gettext', 'ngMaterial', 'cfp.hotkeys']);

//setup error handling
// http://jsnlog.com/Documentation/GetStartedLogging/AngularJsErrorHandling

// Log JavaScript exceptions
angular.module('TylerUI').factory('$exceptionHandler', [function () {
  return function (exception, cause) {
    JL('Angular').fatalException(cause, exception);
    throw exception;
  };
}]);


// Log AJAX issues
angular.module("TylerUI").config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
    return {
      'request': function (config) {
        config.msBeforeAjaxCall = new Date().getTime();
        return config;
      },
      'response': function (response) {
        if (response.config.warningAfter) {
          var msAfterAjaxCall = new Date().getTime();
          var timeTakenInMs =
                msAfterAjaxCall - response.config.msBeforeAjaxCall;
          if (timeTakenInMs > response.config.warningAfter) {
            JL('Angular.Ajax').warn({
              timeTakenInMs: timeTakenInMs,
              config: response.config,
              data: response.data
            });
          }
        }
        return response;
      },
      'responseError': function (rejection) {
        var errorMessage = "timeout";
        if (rejection.status !== 0) {
          errorMessage = rejection.data.ExceptionMessage;
        }
        JL('Angular.Ajax').fatalException({
          errorMessage: errorMessage,
          status: rejection.status,
          config: rejection.config
        }, rejection.data);
        return $q.reject(rejection);
      }
    };
  }]);
}]);


// http://jsnlog.com/Documentation/GetStartedLogging/ExceptionLogging
// Logging JavaScript Exceptions
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
  // Send object with all data to server side log, using severity fatal,
  // from logger "onerrorLogger"
  JL("onerrorLogger").fatalException({
    "msg": "Exception!",
    "errorMsg": errorMsg, "url": url,
    "line number": lineNumber, "column": column
  }, errorObj);

  // Tell browser to run its own error handler as well
  return false;
};


// workaround for date picker bug
// https://github.com/angular-ui/bootstrap/issues/2659
angular.module('TylerUI').directive('datepickerPopup', function () {
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function (scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  }
});

